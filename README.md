## Requirements:
- Node: https://nodejs.org/en/download/
- Mysql
---

## Installation:
- Go to project folder
- run: "npm install"
- create a database and import file "carapi.sql"
---

## How to run project:
- open file ".env"
- change database configs: username, password, databasename, etc..
- run: "npx directus start"
- open "http://localhost:8055" on your browser, this will bring you to platform admin dashboard.
- You can use this account: username: admin@example.com - password: 123456
- Go to: http://localhost:8055/admin/settings/data-model, You can see that I have already defined a "Parts" model for you to test.
- Go to: http://localhost:8055/admin/collections, you can view and manage the data of "parts" here.
- Go to: http://localhost:8055/admin/settings/roles to manage roles and permissions
- Go to: http://localhost:8055/admin/users to manage users
---

## How to test API:
- You need Postman: https://www.postman.com/downloads/
- Run postman and import "NSI Test API.postman_collection.json"
- Use "login" request call to get access_token. Check tab "Body" where you can enter email, password. you can use email: tester@mailinator.com - password: 123456
- Use access_token to run other request calls.
---

## More Info:
https://docs.directus.io/
---
