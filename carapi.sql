-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Aug 02, 2021 at 12:54 AM
-- Server version: 5.7.31
-- PHP Version: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `carapi`
--

-- --------------------------------------------------------

--
-- Table structure for table `directus_activity`
--

DROP TABLE IF EXISTS `directus_activity`;
CREATE TABLE IF NOT EXISTS `directus_activity` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `action` varchar(45) NOT NULL,
  `user` char(36) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ip` varchar(50) NOT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `collection` varchar(64) NOT NULL,
  `item` varchar(255) NOT NULL,
  `comment` text,
  PRIMARY KEY (`id`),
  KEY `directus_activity_collection_foreign` (`collection`)
) ENGINE=MyISAM AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_activity`
--

INSERT INTO `directus_activity` (`id`, `action`, `user`, `timestamp`, `ip`, `user_agent`, `collection`, `item`, `comment`) VALUES
(1, 'authenticate', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:05:35', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_users', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', NULL),
(2, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:06:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_settings', '1', NULL),
(3, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:08:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_collections', 'parts', NULL),
(4, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:08:03', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '1', NULL),
(5, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:11:28', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '2', NULL),
(6, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:11:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '2', NULL),
(7, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:14:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '3', NULL),
(8, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:15:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '1', NULL),
(9, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:15:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '3', NULL),
(10, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:15:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '2', NULL),
(11, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:16:08', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '4', NULL),
(12, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:16:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '4', NULL),
(13, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:17:13', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '5', NULL),
(14, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:20:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '6', NULL),
(15, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:20:47', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '7', NULL),
(16, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:21:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '8', NULL),
(17, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:22:40', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '9', NULL),
(18, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:22:50', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '10', NULL),
(19, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:23:06', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '11', NULL),
(20, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:23:22', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '12', NULL),
(21, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:23:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '13', NULL),
(22, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:24:11', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '14', NULL),
(23, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:24:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '15', NULL),
(24, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:24:39', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '16', NULL),
(25, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:25:20', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '17', NULL),
(26, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:25:37', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '18', NULL),
(27, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:26:00', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '19', NULL),
(28, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:26:44', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '20', NULL),
(29, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_roles', 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', NULL),
(30, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '1', NULL),
(31, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '2', NULL),
(32, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '3', NULL),
(33, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '4', NULL),
(34, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '5', NULL),
(35, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '6', NULL),
(36, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '7', NULL),
(37, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '8', NULL),
(38, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '9', NULL),
(39, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '10', NULL),
(40, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:52', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '11', NULL),
(41, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '15', NULL),
(42, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '15', NULL),
(43, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '15', NULL),
(44, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:37:56', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '15', NULL),
(45, 'delete', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:38:01', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '12', NULL),
(46, 'delete', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:38:05', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '14', NULL),
(47, 'delete', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:38:07', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '15', NULL),
(48, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:39:54', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_users', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', NULL),
(49, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:44:12', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '16', NULL),
(50, 'delete', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:44:30', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_permissions', '16', NULL),
(51, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:53:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_collections', 'tests', NULL),
(52, 'create', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:53:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '21', NULL),
(53, 'delete', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:21', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_collections', 'tests', NULL),
(54, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '1', NULL),
(55, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '3', NULL),
(56, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '2', NULL),
(57, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '4', NULL),
(58, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '5', NULL),
(59, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '6', NULL),
(60, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '7', NULL),
(61, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '9', NULL),
(62, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '10', NULL),
(63, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '11', NULL),
(64, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '12', NULL),
(65, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '13', NULL),
(66, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '14', NULL),
(67, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '15', NULL),
(68, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '16', NULL),
(69, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '17', NULL),
(70, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '18', NULL),
(71, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '19', NULL),
(72, 'update', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-01 13:58:48', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36', 'directus_fields', '20', NULL),
(73, 'authenticate', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-01 14:03:55', '::1', 'PostmanRuntime/7.26.8', 'directus_users', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', NULL),
(74, 'authenticate', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-01 14:10:50', '::1', 'PostmanRuntime/7.26.8', 'directus_users', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', NULL),
(75, 'authenticate', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-01 14:10:52', '::1', 'PostmanRuntime/7.26.8', 'directus_users', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', NULL),
(76, 'authenticate', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-01 14:10:53', '::1', 'PostmanRuntime/7.26.8', 'directus_users', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_collections`
--

DROP TABLE IF EXISTS `directus_collections`;
CREATE TABLE IF NOT EXISTS `directus_collections` (
  `collection` varchar(64) NOT NULL,
  `icon` varchar(30) DEFAULT NULL,
  `note` text,
  `display_template` varchar(255) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `singleton` tinyint(1) NOT NULL DEFAULT '0',
  `translations` json DEFAULT NULL,
  `archive_field` varchar(64) DEFAULT NULL,
  `archive_app_filter` tinyint(1) NOT NULL DEFAULT '1',
  `archive_value` varchar(255) DEFAULT NULL,
  `unarchive_value` varchar(255) DEFAULT NULL,
  `sort_field` varchar(64) DEFAULT NULL,
  `accountability` varchar(255) DEFAULT 'all',
  `color` varchar(255) DEFAULT NULL,
  `item_duplication_fields` json DEFAULT NULL,
  PRIMARY KEY (`collection`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_collections`
--

INSERT INTO `directus_collections` (`collection`, `icon`, `note`, `display_template`, `hidden`, `singleton`, `translations`, `archive_field`, `archive_app_filter`, `archive_value`, `unarchive_value`, `sort_field`, `accountability`, `color`, `item_duplication_fields`) VALUES
('parts', NULL, NULL, NULL, 0, 0, NULL, NULL, 1, NULL, NULL, NULL, 'all', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_fields`
--

DROP TABLE IF EXISTS `directus_fields`;
CREATE TABLE IF NOT EXISTS `directus_fields` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `collection` varchar(64) NOT NULL,
  `field` varchar(64) NOT NULL,
  `special` varchar(64) DEFAULT NULL,
  `interface` varchar(64) DEFAULT NULL,
  `options` json DEFAULT NULL,
  `display` varchar(64) DEFAULT NULL,
  `display_options` json DEFAULT NULL,
  `readonly` tinyint(1) NOT NULL DEFAULT '0',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `sort` int(10) UNSIGNED DEFAULT NULL,
  `width` varchar(30) DEFAULT 'full',
  `group` int(10) UNSIGNED DEFAULT NULL,
  `translations` json DEFAULT NULL,
  `note` text,
  `conditions` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_fields_collection_foreign` (`collection`),
  KEY `directus_fields_group_foreign` (`group`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_fields`
--

INSERT INTO `directus_fields` (`id`, `collection`, `field`, `special`, `interface`, `options`, `display`, `display_options`, `readonly`, `hidden`, `sort`, `width`, `group`, `translations`, `note`, `conditions`) VALUES
(2, 'parts', 'description', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 3, 'full', NULL, NULL, NULL, NULL),
(3, 'parts', 'part_print', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 2, 'full', NULL, NULL, NULL, NULL),
(4, 'parts', 'status', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 4, 'full', NULL, NULL, NULL, NULL),
(5, 'parts', 'line', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 5, 'full', NULL, NULL, NULL, NULL),
(6, 'parts', 'vc1', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 6, 'full', NULL, NULL, NULL, NULL),
(7, 'parts', 'brand1', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 7, 'full', NULL, NULL, NULL, NULL),
(9, 'parts', 'oh8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 8, 'full', NULL, NULL, NULL, NULL),
(10, 'parts', 'mtd_sls8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 9, 'full', NULL, NULL, NULL, NULL),
(11, 'parts', 'prr_sls8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 10, 'full', NULL, NULL, NULL, NULL),
(12, 'parts', 'pr2_sls8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 11, 'full', NULL, NULL, NULL, NULL),
(13, 'parts', 'sls_12m8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 12, 'full', NULL, NULL, NULL, NULL),
(14, 'parts', 'bin81', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 13, 'full', NULL, NULL, NULL, NULL),
(15, 'parts', 'bin82', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 14, 'full', NULL, NULL, NULL, NULL),
(16, 'parts', 'bin83', NULL, 'input', '{\"trim\": true, \"iconLeft\": null, \"iconRight\": null}', 'raw', NULL, 0, 0, 15, 'full', NULL, NULL, NULL, NULL),
(17, 'parts', 'trr8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 16, 'full', NULL, NULL, NULL, NULL),
(18, 'parts', 'avl8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 17, 'full', NULL, NULL, NULL, NULL),
(19, 'parts', 'o_o8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 18, 'full', NULL, NULL, NULL, NULL),
(20, 'parts', 'i_t8', NULL, 'input', '{\"iconLeft\": null, \"iconRight\": null}', NULL, NULL, 0, 0, 19, 'full', NULL, NULL, NULL, NULL),
(1, 'parts', 'part', NULL, 'input', NULL, NULL, NULL, 0, 0, 1, 'full', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_files`
--

DROP TABLE IF EXISTS `directus_files`;
CREATE TABLE IF NOT EXISTS `directus_files` (
  `id` char(36) NOT NULL,
  `storage` varchar(255) NOT NULL,
  `filename_disk` varchar(255) DEFAULT NULL,
  `filename_download` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `folder` char(36) DEFAULT NULL,
  `uploaded_by` char(36) DEFAULT NULL,
  `uploaded_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` char(36) DEFAULT NULL,
  `modified_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `charset` varchar(50) DEFAULT NULL,
  `filesize` bigint(20) DEFAULT NULL,
  `width` int(10) UNSIGNED DEFAULT NULL,
  `height` int(10) UNSIGNED DEFAULT NULL,
  `duration` int(10) UNSIGNED DEFAULT NULL,
  `embed` varchar(200) DEFAULT NULL,
  `description` text,
  `location` text,
  `tags` text,
  `metadata` json DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_files_uploaded_by_foreign` (`uploaded_by`),
  KEY `directus_files_modified_by_foreign` (`modified_by`),
  KEY `directus_files_folder_foreign` (`folder`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directus_folders`
--

DROP TABLE IF EXISTS `directus_folders`;
CREATE TABLE IF NOT EXISTS `directus_folders` (
  `id` char(36) NOT NULL,
  `name` varchar(255) NOT NULL,
  `parent` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_folders_parent_foreign` (`parent`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directus_migrations`
--

DROP TABLE IF EXISTS `directus_migrations`;
CREATE TABLE IF NOT EXISTS `directus_migrations` (
  `version` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`version`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_migrations`
--

INSERT INTO `directus_migrations` (`version`, `name`, `timestamp`) VALUES
('20201028A', 'Remove Collection Foreign Keys', '2021-08-01 13:03:24'),
('20201029A', 'Remove System Relations', '2021-08-01 13:03:24'),
('20201029B', 'Remove System Collections', '2021-08-01 13:03:24'),
('20201029C', 'Remove System Fields', '2021-08-01 13:03:24'),
('20201105A', 'Add Cascade System Relations', '2021-08-01 13:03:24'),
('20201105B', 'Change Webhook URL Type', '2021-08-01 13:03:24'),
('20210225A', 'Add Relations Sort Field', '2021-08-01 13:03:24'),
('20210304A', 'Remove Locked Fields', '2021-08-01 13:03:24'),
('20210312A', 'Webhooks Collections Text', '2021-08-01 13:03:24'),
('20210331A', 'Add Refresh Interval', '2021-08-01 13:03:24'),
('20210415A', 'Make Filesize Nullable', '2021-08-01 13:03:24'),
('20210416A', 'Add Collections Accountability', '2021-08-01 13:03:24'),
('20210422A', 'Remove Files Interface', '2021-08-01 13:03:24'),
('20210506A', 'Rename Interfaces', '2021-08-01 13:03:24'),
('20210510A', 'Restructure Relations', '2021-08-01 13:03:25'),
('20210518A', 'Add Foreign Key Constraints', '2021-08-01 13:03:38'),
('20210519A', 'Add System Fk Triggers', '2021-08-01 13:03:39'),
('20210521A', 'Add Collections Icon Color', '2021-08-01 13:03:39'),
('20210608A', 'Add Deep Clone Config', '2021-08-01 13:03:39'),
('20210626A', 'Change Filesize Bigint', '2021-08-01 13:03:39'),
('20210716A', 'Add Conditions to Fields', '2021-08-01 13:03:39'),
('20210721A', 'Add Default Folder', '2021-08-01 13:03:39');

-- --------------------------------------------------------

--
-- Table structure for table `directus_permissions`
--

DROP TABLE IF EXISTS `directus_permissions`;
CREATE TABLE IF NOT EXISTS `directus_permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role` char(36) DEFAULT NULL,
  `collection` varchar(64) NOT NULL,
  `action` varchar(10) NOT NULL,
  `permissions` json DEFAULT NULL,
  `validation` json DEFAULT NULL,
  `presets` json DEFAULT NULL,
  `fields` text,
  `limit` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_permissions_collection_foreign` (`collection`),
  KEY `directus_permissions_role_foreign` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_permissions`
--

INSERT INTO `directus_permissions` (`id`, `role`, `collection`, `action`, `permissions`, `validation`, `presets`, `fields`, `limit`) VALUES
(1, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_files', 'create', '{}', NULL, NULL, '*', NULL),
(2, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_files', 'read', '{}', NULL, NULL, '*', NULL),
(3, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_files', 'update', '{}', NULL, NULL, '*', NULL),
(4, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_files', 'delete', '{}', NULL, NULL, '*', NULL),
(5, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_folders', 'create', '{}', NULL, NULL, '*', NULL),
(6, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_folders', 'read', '{}', NULL, NULL, '*', NULL),
(7, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_folders', 'update', '{}', NULL, NULL, '*', NULL),
(8, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_folders', 'delete', '{}', NULL, NULL, NULL, NULL),
(9, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_users', 'read', '{}', NULL, NULL, NULL, NULL),
(10, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_users', 'update', '{\"id\": {\"_eq\": \"$CURRENT_USER\"}}', NULL, NULL, 'first_name,last_name,email,password,location,title,description,avatar,language,theme', NULL),
(11, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'directus_roles', 'read', '{}', NULL, NULL, '*', NULL),
(13, 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'parts', 'read', '{}', '{}', NULL, '*', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_presets`
--

DROP TABLE IF EXISTS `directus_presets`;
CREATE TABLE IF NOT EXISTS `directus_presets` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bookmark` varchar(255) DEFAULT NULL,
  `user` char(36) DEFAULT NULL,
  `role` char(36) DEFAULT NULL,
  `collection` varchar(64) DEFAULT NULL,
  `search` varchar(100) DEFAULT NULL,
  `filters` json DEFAULT NULL,
  `layout` varchar(100) DEFAULT 'tabular',
  `layout_query` json DEFAULT NULL,
  `layout_options` json DEFAULT NULL,
  `refresh_interval` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_presets_collection_foreign` (`collection`),
  KEY `directus_presets_user_foreign` (`user`),
  KEY `directus_presets_role_foreign` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_presets`
--

INSERT INTO `directus_presets` (`id`, `bookmark`, `user`, `role`, `collection`, `search`, `filters`, `layout`, `layout_query`, `layout_options`, `refresh_interval`) VALUES
(1, NULL, '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', NULL, 'parts', NULL, NULL, 'tabular', '{\"tabular\": {\"fields\": [\"part_number\", \"part_print\", \"description\", \"status\", \"line\", \"vc1\", \"brand1\", \"oh8\", \"pr2_sls8\", \"sls_12m8\", \"bin81\", \"bin82\", \"bin83\", \"trr8\", \"avl8\", \"o_o8\", \"i_t8\", \"mtd_sls8\", \"prr_sls8\"]}}', NULL, NULL),
(2, NULL, '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', NULL, 'directus_files', NULL, NULL, 'cards', '{\"cards\": {\"page\": 1, \"sort\": \"-uploaded_on\"}}', '{\"cards\": {\"icon\": \"insert_drive_file\", \"size\": 4, \"title\": \"{{ title }}\", \"imageFit\": \"crop\", \"subtitle\": \"{{ type }} • {{ filesize }}\"}}', NULL),
(3, NULL, '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', NULL, 'directus_users', NULL, NULL, 'cards', '{\"cards\": {\"page\": 1, \"sort\": \"email\"}}', '{\"cards\": {\"icon\": \"account_circle\", \"size\": 4, \"title\": \"{{ first_name }} {{ last_name }}\", \"subtitle\": \"{{ email }}\"}}', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_relations`
--

DROP TABLE IF EXISTS `directus_relations`;
CREATE TABLE IF NOT EXISTS `directus_relations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `many_collection` varchar(64) NOT NULL,
  `many_field` varchar(64) NOT NULL,
  `one_collection` varchar(64) DEFAULT NULL,
  `one_field` varchar(64) DEFAULT NULL,
  `one_collection_field` varchar(64) DEFAULT NULL,
  `one_allowed_collections` text,
  `junction_field` varchar(64) DEFAULT NULL,
  `sort_field` varchar(64) DEFAULT NULL,
  `one_deselect_action` varchar(255) NOT NULL DEFAULT 'nullify',
  PRIMARY KEY (`id`),
  KEY `directus_relations_many_collection_foreign` (`many_collection`),
  KEY `directus_relations_one_collection_foreign` (`one_collection`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directus_revisions`
--

DROP TABLE IF EXISTS `directus_revisions`;
CREATE TABLE IF NOT EXISTS `directus_revisions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `activity` int(10) UNSIGNED NOT NULL,
  `collection` varchar(64) NOT NULL,
  `item` varchar(255) NOT NULL,
  `data` json DEFAULT NULL,
  `delta` json DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_revisions_collection_foreign` (`collection`),
  KEY `directus_revisions_parent_foreign` (`parent`),
  KEY `directus_revisions_activity_foreign` (`activity`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `directus_roles`
--

DROP TABLE IF EXISTS `directus_roles`;
CREATE TABLE IF NOT EXISTS `directus_roles` (
  `id` char(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `icon` varchar(30) NOT NULL DEFAULT 'supervised_user_circle',
  `description` text,
  `ip_access` text,
  `enforce_tfa` tinyint(1) NOT NULL DEFAULT '0',
  `module_list` json DEFAULT NULL,
  `collection_list` json DEFAULT NULL,
  `admin_access` tinyint(1) NOT NULL DEFAULT '0',
  `app_access` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_roles`
--

INSERT INTO `directus_roles` (`id`, `name`, `icon`, `description`, `ip_access`, `enforce_tfa`, `module_list`, `collection_list`, `admin_access`, `app_access`) VALUES
('28857d0a-34a0-4189-8ff0-df95f17747c3', 'Administrator', 'verified', 'Initial administrative role with unrestricted App/API access', NULL, 0, NULL, NULL, 1, 1),
('b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', 'Normal User', 'supervised_user_circle', NULL, NULL, 0, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `directus_sessions`
--

DROP TABLE IF EXISTS `directus_sessions`;
CREATE TABLE IF NOT EXISTS `directus_sessions` (
  `token` varchar(64) NOT NULL,
  `user` char(36) NOT NULL,
  `expires` timestamp NOT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`token`),
  KEY `directus_sessions_user_foreign` (`user`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_sessions`
--

INSERT INTO `directus_sessions` (`token`, `user`, `expires`, `ip`, `user_agent`) VALUES
('FGJirGMTuOzlLmG3FZQLTUfB2ejCVMiPhXulbq_-4kmEarVjqAbyxsjEmmAVdzpJ', '1de49f4c-5488-4d5a-96b4-bcba2fae31ae', '2021-08-08 14:13:25', '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36'),
('kFhKIuizYhiEQdtNdSr_LZrSo7ee0uelRyJbQ8Qy3MkhqmzJYVuYmGbq4Ta6pIPT', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-08 14:03:56', '::1', 'PostmanRuntime/7.26.8'),
('9RwIYyuaoB2yoytpisa6U2OUzMz4eNBHynYIlQq6SLBhCq3Gk_R4qTHLjRCAv13M', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-08 14:10:51', '::1', 'PostmanRuntime/7.26.8'),
('-h0faZoeVTdHR6QC4B-u1nU6kVJmXDFuXbpM-7J7DLRuKwyHNlx7EgCOjbqrIj5B', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-08 14:10:53', '::1', 'PostmanRuntime/7.26.8'),
('hk0D2sefSYwp7iOJ51veI7O_8AMx7_YVjLzUJH_PL-VWDIzt5yCLiDTmrRG4WqzA', '0fb5a451-1804-4895-ab15-8c927ff2a1f8', '2021-08-08 14:10:54', '::1', 'PostmanRuntime/7.26.8');

-- --------------------------------------------------------

--
-- Table structure for table `directus_settings`
--

DROP TABLE IF EXISTS `directus_settings`;
CREATE TABLE IF NOT EXISTS `directus_settings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL DEFAULT 'Directus',
  `project_url` varchar(255) DEFAULT NULL,
  `project_color` varchar(10) DEFAULT '#00C897',
  `project_logo` char(36) DEFAULT NULL,
  `public_foreground` char(36) DEFAULT NULL,
  `public_background` char(36) DEFAULT NULL,
  `public_note` text,
  `auth_login_attempts` int(10) UNSIGNED DEFAULT '25',
  `auth_password_policy` varchar(100) DEFAULT NULL,
  `storage_asset_transform` varchar(7) DEFAULT 'all',
  `storage_asset_presets` json DEFAULT NULL,
  `custom_css` text,
  `storage_default_folder` char(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `directus_settings_project_logo_foreign` (`project_logo`),
  KEY `directus_settings_public_foreground_foreign` (`public_foreground`),
  KEY `directus_settings_public_background_foreign` (`public_background`),
  KEY `directus_settings_storage_default_folder_foreign` (`storage_default_folder`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_settings`
--

INSERT INTO `directus_settings` (`id`, `project_name`, `project_url`, `project_color`, `project_logo`, `public_foreground`, `public_background`, `public_note`, `auth_login_attempts`, `auth_password_policy`, `storage_asset_transform`, `storage_asset_presets`, `custom_css`, `storage_default_folder`) VALUES
(1, 'NSI Test Api', NULL, '#00C897', NULL, NULL, NULL, NULL, 25, NULL, 'all', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_users`
--

DROP TABLE IF EXISTS `directus_users`;
CREATE TABLE IF NOT EXISTS `directus_users` (
  `id` char(36) NOT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `description` text,
  `tags` json DEFAULT NULL,
  `avatar` char(36) DEFAULT NULL,
  `language` varchar(8) DEFAULT 'en-US',
  `theme` varchar(20) DEFAULT 'auto',
  `tfa_secret` varchar(255) DEFAULT NULL,
  `status` varchar(16) NOT NULL DEFAULT 'active',
  `role` char(36) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `last_access` timestamp NULL DEFAULT NULL,
  `last_page` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `directus_users_email_unique` (`email`),
  KEY `directus_users_role_foreign` (`role`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `directus_users`
--

INSERT INTO `directus_users` (`id`, `first_name`, `last_name`, `email`, `password`, `location`, `title`, `description`, `tags`, `avatar`, `language`, `theme`, `tfa_secret`, `status`, `role`, `token`, `last_access`, `last_page`) VALUES
('1de49f4c-5488-4d5a-96b4-bcba2fae31ae', 'Admin', 'User', 'admin@example.com', '$argon2i$v=19$m=4096,t=3,p=1$hQ1BKBSRc+QDfbmZekGWEA$lFK/hJcCzTu8a2mz3ROq70j/9r/Gq/ZLHQEj+PzlqLc', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', '28857d0a-34a0-4189-8ff0-df95f17747c3', NULL, '2021-08-01 14:13:25', '/docs/guides/projects'),
('0fb5a451-1804-4895-ab15-8c927ff2a1f8', 'Test', 'Test', 'tester@mailinator.com', '$argon2i$v=19$m=4096,t=3,p=1$DFjhz5W+DxFnAT9R7VZcIg$gbt1iQP14QNW4Wm0VsZVemSYFVBhpwg4TMw+W7HNkcY', NULL, NULL, NULL, NULL, NULL, 'en-US', 'auto', NULL, 'active', 'b964c8b3-14f9-40f6-8e4a-4b8fa3b338e3', NULL, '2021-08-01 14:10:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `directus_webhooks`
--

DROP TABLE IF EXISTS `directus_webhooks`;
CREATE TABLE IF NOT EXISTS `directus_webhooks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `method` varchar(10) NOT NULL DEFAULT 'POST',
  `url` text,
  `status` varchar(10) NOT NULL DEFAULT 'active',
  `data` tinyint(1) NOT NULL DEFAULT '1',
  `actions` varchar(100) NOT NULL,
  `collections` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parts`
--

DROP TABLE IF EXISTS `parts`;
CREATE TABLE IF NOT EXISTS `parts` (
  `part` varchar(255) NOT NULL,
  `part_print` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `line` varchar(255) DEFAULT NULL,
  `vc1` varchar(255) DEFAULT NULL,
  `brand1` varchar(255) DEFAULT NULL,
  `oh8` int(11) DEFAULT NULL,
  `mtd_sls8` int(11) DEFAULT NULL,
  `prr_sls8` int(11) DEFAULT NULL,
  `pr2_sls8` int(11) DEFAULT NULL,
  `sls_12m8` int(11) DEFAULT NULL,
  `bin81` varchar(255) DEFAULT NULL,
  `bin82` varchar(255) DEFAULT NULL,
  `bin83` varchar(255) DEFAULT NULL,
  `trr8` int(11) DEFAULT NULL,
  `avl8` int(11) DEFAULT NULL,
  `o_o8` int(11) DEFAULT NULL,
  `i_t8` int(11) DEFAULT NULL,
  PRIMARY KEY (`part`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `parts`
--

INSERT INTO `parts` (`part`, `part_print`, `description`, `status`, `line`, `vc1`, `brand1`, `oh8`, `mtd_sls8`, `prr_sls8`, `pr2_sls8`, `sls_12m8`, `bin81`, `bin82`, `bin83`, `trr8`, `avl8`, `o_o8`, `i_t8`) VALUES
('172', '000000 000172', 'CLEARANCE BULB-12V 6W', 'ACT', 'MIS', 'FLO', 'FLOSSER', 169, 16, 0, 12, 152, 'HWL2E', '', '', 0, 169, 0, 0),
('276', '000000 000276', 'BOLT - M8x20', 'ACT', 'MER', 'FA1', 'FISCHER AUTOMOTIVE', 5, 0, 0, 0, 0, '2103DA', '', '', 0, 5, 0, 0),
('316', '000000 000316', '200A FUSE - VIOLET', 'ACT', 'SPR', 'FLO', 'FLOSSER', 8, 0, 0, 0, 0, 'HWL1AB', '', '', 0, 8, 0, 0),
('413', '000000 000413', '70A FUSE - BROWN', 'ACT', 'SPR', 'FLO', 'FLOSSER', 20, 0, 0, 0, 0, 'HWL4AB', '', '', 0, 20, 0, 0),
('414', '000000 000414', '80A FUSE - WHITE', 'ACT', 'SPR', 'FLO', 'FLOSSER', 6, 0, 0, 0, 3, 'HWL4AB', '', '', 0, 6, 0, 0),
('415', '000000 000415', '100A FUSE - BLUE', 'ACT', 'SPR', 'FLO', 'FLOSSER', 0, 0, 0, 0, 0, '2306JQ', '', '', 0, 0, 0, 0),
('416', '000000 000416', '125A FUSE - PINK', 'ACT', 'SPR', 'FLO', 'FLOSSER', 8, 0, 0, 0, 0, 'HWL5BA', '', '', 0, 8, 0, 0),
('417', '000000 000417', '40A FUSE  - GREEN', 'ACT', 'SPR', 'FLO', 'FLOSSER', 10, 0, 0, 0, 0, '', '', '', 0, 10, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
